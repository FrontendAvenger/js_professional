/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    }
  };

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // false
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/

let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    },
    obj: {
      obj: {
        obj: {
          obj: {
            cat: 'meaw'
          }
        }
      }
    }
  };

const DeepFreeze = ( obj ) => {

  if( typeof obj === 'object'){
        for (let key in obj){
          if (typeof obj[key] == 'object' && obj[key] !== null){
            DeepFreeze( obj[key] );
          } 
        }
        return Object.freeze( obj );
  }
 
}


const work = () => {
  console.log( 'your code 123 ');
  let FarGalaxy = DeepFreeze(universe);

  console.log( FarGalaxy )
      FarGalaxy.good.push('javascript'); // false
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false
      FarGalaxy.obj.obj.obj.obj.cat = 'woof';
}


export default work;
