import Calc, {AddCommand, SubCommand, MultiplyCommand, DivideCommand} from './calculator'

const Calc = new Calc(); 

test('test of calculator', () => {
  Calc.currentValue = 0;
  Calc.execute( new AddCommand( 100 ) );
  Calc.execute( new SubCommand( 10 ) );
  Calc.execute( new MultiplyCommand( 2 ) );
  Calc.execute( new DivideCommand( 3 ) );
  expect( Calc.getCurrentValue() ).toBe(60);
})
test('F-n undo', () => {
  let previousVal = Calc.currentValue;
  Calc.execute( new SubCommand( 15 ) );
  Calc.undo();
  expect( Calc.getCurrentValue() ).toBe(previousVal);
  Calc.execute( new AddCommand( 12 ) );
  Calc.undo();
  expect( Calc.getCurrentValue() ).toBe(previousVal);
  Calc.execute( new MultiplyCommand( 5 ) );
  Calc.undo();
  expect( Calc.getCurrentValue() ).toBe(previousVal);
  Calc.execute( new DivideCommand( 8 ) );
  Calc.undo();
  expect( Calc.getCurrentValue() ).toBe(previousVal);
})

