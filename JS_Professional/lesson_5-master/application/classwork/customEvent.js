/*
  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в
            соответсвии с правилавми ниже:

  1. Написать кастомные события которые будут менять статус светофора:
  - start: включает зеленый свет
  - stop: включает красный свет
  - night: включает желтый свет, который моргает с интервалом в 1с.
  И зарегистрировать каждое через addEventListener на каждом из светофоров.

  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,
      чтобы включить режим "нерегулируемого перекрестка" (моргающий желтый).

  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)
      или зеленый (на второй клик) цвет соотвественно.
      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.

  4.  + Бонус: На кнопку "Start Night" повесить сброс всех светофоров с их текущего
      статуса, на мигающий желтые.
      Двойной, тройной и более клики на кнопку не должны вызывать повторную
      инициализацию инвервала.

*/

const roadTraffic = () => {
  let trafficLights = document.querySelectorAll('.trafficLight');
  let btn = document.getElementById("Do");

  let nightEvent = new CustomEvent("startYellow");

  document.addEventListener("DOMContentLoaded", () => {
    trafficLights.forEach( (item) => {
      item.dispatchEvent(nightEvent);
    })
  });

  trafficLights.forEach( (item) => {
    item.addEventListener('startYellow', function(e){
      let timer = () => {
        
        if(item.classList.contains('red') == false && item.classList.contains('green') == false){
            item.classList.toggle('yellow');
         } else {
          clearInterval(MyInterval)
         }
      }
      let MyInterval = setInterval(timer,1000); 
    })
  })

  trafficLights.forEach( (item) => {
    item.addEventListener('click', (e) => {
      if (item.classList.contains('red') == false){
        console.log("Event" , e);
        let stopEvent = new CustomEvent('startRed',{detail: {
          id : e.target.getAttribute("id")
            }
        });
        e.target.dispatchEvent(stopEvent);
      
     }else {
        let startEvent = new CustomEvent('startGreen',{detail: {
            id : e.target.getAttribute("id")
          }
        });
        e.target.dispatchEvent(startEvent);
      } 
    })
  })

  trafficLights.forEach( (item) => {
    item.addEventListener('startRed', function(event){
          console.log(event);
          let {id} = event.detail;
          let currentLight = document.getElementById(id);
          
          if (currentLight.classList.contains('yellow')){
            currentLight.classList.remove('yellow');
          }

          if (currentLight.classList.contains('green')){
            currentLight.classList.remove('green');
          }

          currentLight.classList.add('red');
        })

    item.addEventListener('startGreen', function(){
          let {id} = event.detail;
          let currentLight = document.getElementById(id);
          currentLight.classList.remove('red');
          currentLight.classList.add('green');
        })
   })
}
export default roadTraffic